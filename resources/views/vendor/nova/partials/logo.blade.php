@if (isset($width))
    <!-- Login logo -->
    <img src="{{ asset('images/actual-azul.png') }}" width="{{ $width ?? '126' }}" alt="">
@else
    <!-- Sidebar logo -->
    <img src="{{ asset('images/actual.png') }}" width="{{ $width ?? '126' }}" alt="">
@endif
