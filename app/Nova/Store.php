<?php

namespace App\Nova;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Timothyasp\Color\Color;

class Store extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Store';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    public static $group = 'Administración';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Nombre', 'name')
                ->rules(['required']),

            HasMany::make('Categorias', 'categories', Category::class)
                ->rules(['required']),

            Image::make('Fondo iPad', 'background_ipad')
                ->creationRules(['required']),

            Image::make('Logo', 'logo')
                ->creationRules(['required']),

            Number::make('Tiempo para volver al home', 'back_to_home_timeout')
                ->help('La cantidad de segundos que la aplicación esperará antes de volver al inicio si no hay interacción'),

            Color::make('Color de botones', 'button_color')->slider(),

            Color::make('Color de texto del botón', 'button_text_color')->slider(),

            BelongsTo::make('Categoría de inicio', 'slider_category', Category::class)
                ->nullable()
                ->help('Categoría que se utilizará para mostrar contenido en modo "slider" mientras no se controla manualmente el contenido en la pantalla')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function singularLabel()
    {
        return 'Sala de venta';
    }

    public static function label()
    {
        return 'Salas de venta';
    }
}
