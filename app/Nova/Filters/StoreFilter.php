<?php

namespace App\Nova\Filters;

use App\Store;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class StoreFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->leftJoin('categories', 'media.category_id', '=', 'categories.id')
            ->where('categories.store_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return Store::pluck('id', 'name');
    }
}
