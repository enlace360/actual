<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;

class SyncDataController extends Controller
{
    public function sync(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'store_id' => 'required|exists:stores,id'
        ]);

        if ($request->get('token') !== config('app.backup_token')) {
            return response()->json(['Unauthorized'])->setStatusCode(403);
        }

        return response()->json([
            'data' => Store::find($request->store_id)->load('categories', 'categories.media')
        ]);
    }
}
