<?php

namespace App\Console\Commands;

use App\Category;
use App\Media;
use App\Store;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class UpdateTask extends Command
{
    protected $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore backup from internet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $thisStoreId = config('app.store_id');
        if (!$thisStoreId) {
            $this->error('This instance of the app is not a store. Add STORE_ID to .env');
            return;
        }
        $this->client = new Client([
            'base_uri' => config('app.backup_endpoint'),
            'headers' => [
                'Accept' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest'
            ]
        ]);
        $newStoreData = $this->getDataFromAPI();
        $store = Store::find($thisStoreId);
        if ($store && Carbon::parse($newStoreData->updated_at)->lt($store->updated_at) ) {
            $this->info('There is no new data. Skipping update.');
            return;
        }
        \DB::transaction(function () use ($newStoreData) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            \DB::table('media')->truncate();
            \DB::table('categories')->truncate();
            \DB::table('stores')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            $this->syncData($newStoreData);
        });

        $media = Media::get();

        foreach ($media as $medium) {
            $this->downloadFile([$medium->source, $medium->image]);
        }

        $this->notifySocket();

        $this->info('======================================');
        $this->info('Process complete 🙌');
        $this->info('======================================');
    }

    /**
     * @param $fileNames
     */
    protected function downloadFile($fileNames)
    {
        $fileNames = is_array($fileNames) ? $fileNames : [$fileNames];

        foreach ($fileNames as $fileName) {
            if (!$fileName) continue;
            $path = storage_path('app/public/' . $fileName);
            if (file_exists($path)) {
                $this->info('File already exists: ' . $path);
                continue;
            }
            $this->client
                ->get('/storage/' . $fileName, [
                    'save_to' => storage_path('app/public/' . $fileName)
                ]);

            $this->info('Image downloaded: ' . $fileName);
        }
    }

    /**
     * @return $this
     */
    protected function syncData($store)
    {
        Store::unguard();
        $newStore = Store::create([
            'id' => $store->id,
            'name' => $store->name,
            'background_ipad' => $store->background_ipad,
            'logo' => $store->logo,
            'back_to_home_timeout' => $store->back_to_home_timeout,
            'button_color' => $store->button_color,
            'button_text_color' => $store->button_text_color,
            'created_at' => $store->created_at,
            'updated_at' => $store->updated_at,
        ]);
        $this->downloadFile([$store->background_ipad, $store->logo]);
        foreach ($store->categories as $category) {
            $media = $category->media;

            Category::create([
                'id' => $category->id,
                'name' => $category->name,
                'store_id' => $category->store_id
            ]);

            $this->info('Created category ' . $category->name . ' (' . $category->id . ')');

            foreach ($media as $medium) {
                Media::create([
                    "id" => $medium->id,
                    "title" => $medium->title,
                    "description" => $medium->description,
                    "source" => $medium->source,
                    "category_id" => $medium->category_id,
                    "created_at" => $medium->created_at,
                    "updated_at" => $medium->updated_at,
                    "type" => $medium->type,
                    "image" => $medium->image
                ]);

                $this->info('Created medium ' . $medium->title . ' (' . $medium->id . ')');
            }
        }
        $newStore->timestamps = false;
        $newStore->slider_category_id = $store->slider_category_id;
        $newStore->save();


        return $this;
    }
    /**
     * @return mixed
     */
    protected function getDataFromAPI()
    {
        $response = json_decode($this->client->get('sync', [
                'query' => [
                    'token' => config('app.backup_token'),
                    'store_id' => config('app.store_id')
                ]
            ])->getBody()->getContents());
        $store = $response->data;

        return $store;
    }
    protected function notifySocket(): void
    {
        try {
            $socketClient = new Client([
                'base_uri' => config('app.socket_server'),
            ]);
            $response = $socketClient->get('/force-client-update');
            $this->info('Socket notified: ' . $response->getBody()->getContents());
        } catch (\Exception $e) {
            $this->error('Could not notify socket' . $e->getMessage());
        }
    }

}
